package com.aait.ejar.Network

import com.aait.ejar.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {
    @POST("about")
    fun About(@Header("lang") lang: String): Call<TermsResponse>

    @POST("terms")
    fun Terms(@Header("lang") lang: String): Call<TermsResponse>

    @FormUrlEncoded
    @POST("sign-up")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("email") email:String?,
               @Field("address") address:String,
               @Field("lat") lat:String,
               @Field("lng") lng:String,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String): Call<UserResponse>

    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>
    @FormUrlEncoded
    @POST("forget-password")
    fun ForGot(@Field("phone") phone:String,
               @Header("lang") lang:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Field("password") password:String,
                @Field("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("edit-profile")
    fun getProfile(@Header("Authorization") Authorization:String,
                   @Header("lang") lang:String,
                   @Field("name") name:String?,
                   @Field("phone") phone: String?,
                   @Field("email") email: String?,
                   @Field("address") address:String?,
                   @Field("lat") lat:String?,
                   @Field("lng") lng:String?):Call<UserResponse>
    @FormUrlEncoded
    @POST("reset-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("current_password") current_password:String,
                      @Field("password") password:String):Call<BaseResponse>
    @Multipart
    @POST("edit-profile")
    fun AddImage(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Part avatar: MultipartBody.Part):Call<UserResponse>

    @POST("home")
    fun Home(@Header("lang") lang:String,
             @Query("lat") lat:String,
             @Query("lng") lng: String,
             @Query("category_id") category_id:Int?,
             @Query("text_search") text_search:String?):Call<HomeResponse>

    @POST("property-details")
    fun Details(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String?,
                @Query("property_id") property_id:Int):Call<PropertyDetailsResponse>

    @POST("categories")
    fun Categories(@Header("lang") lang:String):Call<ListResponse>

    @Multipart
    @POST("add-property")
    fun AddProperty(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
    @Query("category_id") category_id:Int,
    @Query("property_space") property_space:String,
    @Query("address") address:String,
    @Query("lat") lat:String,
    @Query("lng") lng:String,
    @Query("neighborhood_ar") neighborhood_ar:String,
    @Query("neighborhood_en") neighborhood_en:String,
    @Query("number_rooms") number_rooms:String,
    @Query("number_roles") number_roles:String,
    @Query("annual_amount") annual_amount:String,
    @Query("monthly_amount") monthly_amount:String,
    @Part images:ArrayList<MultipartBody.Part>):Call<BaseResponse>

    @POST("my-ads")
    fun MyAds(@Header("lang") lang: String,
              @Header("Authorization") Authorization:String,
    @Query("active") active:Int):Call<AdsResponse>

    @POST("edit-property")
    fun editProperty(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String,
                     @Query("property_id") property_id:Int,
                     @Query("category_id") category_id:Int?,
                     @Query("property_space") property_space:String?,
                     @Query("address") address:String?,
                     @Query("lat") lat:String?,
                     @Query("lng") lng:String?,
                     @Query("neighborhood_ar") neighborhood_ar:String?,
                     @Query("neighborhood_en") neighborhood_en:String?,
                     @Query("number_rooms") number_rooms:String?,
                     @Query("number_roles") number_roles:String?,
                     @Query("annual_amount") annual_amount:String?,
                     @Query("monthly_amount") monthly_amount:String?,
                     @Query("delete_images") delete_images:String?):Call<AdsDetailsResponse>

    @Multipart
    @POST("edit-property")
    fun editProperty(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String,
                     @Query("property_id") property_id:Int,
                     @Query("category_id") category_id:Int?,
                     @Query("property_space") property_space:String?,
                     @Query("address") address:String?,
                     @Query("lat") lat:String?,
                     @Query("lng") lng:String?,
                     @Query("neighborhood_ar") neighborhood_ar:String?,
                     @Query("neighborhood_en") neighborhood_en:String?,
                     @Query("number_rooms") number_rooms:String?,
                     @Query("number_roles") number_roles:String?,
                     @Query("annual_amount") annual_amount:String?,
                     @Query("monthly_amount") monthly_amount:String?,
                     @Query("delete_images") delete_images:String?,
                     @Part images:ArrayList<MultipartBody.Part>):Call<AdsDetailsResponse>

    @POST("delete-property")
    fun DeleteProperty(@Header("lang") lang: String,
                       @Header("Authorization") Authorization:String,
                       @Query("property_id") property_id:Int):Call<TermsResponse>
    @POST("send-message")
    fun Send(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("type") type:String,
             @Query("message") message:String):Call<SendChatResponse>

    @POST("conversation")
    fun Conversation(@Query("lang") lang: String,
                     @Query("conversation_id") conversation_id:Int,
                     @Header("Authorization") Authorization:String,
                     @Query("page") page:Int,
                     @Query("app_type") app_type:String):Call<ChatResponse>
    @FormUrlEncoded
    @POST("conversation-id")
    fun Conversation(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Field("receiver_id") receiver_id:Int,
                     @Field("property_id") property_id:Int):Call<ConversationIdResponse>
    @POST("my-conversations")
    fun Conversations(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String):Call<ChatsResponse>

    @POST("notifications")
    fun Notification(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(
            @Header("Authorization") Authorization:String,
            @Header("lang") lang:String,
            @Query("notification_id") notification_id:Int):Call<TermsResponse>
    @POST("contact-us")
    fun CallUs(@Header("lang") lang:String,
    @Query("message") message:String?,
    @Query("name") name: String?,
    @Query("phone") phone: String?):Call<CallUsResponse>

    @POST("switch-notification")
    fun Switch(@Header("Authorization") Authorization:String,
               @Query("switch") switch:Int?):Call<NotifyResponse>
    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Header("lang") lang:String):Call<TermsResponse>

    @POST("orders")
    fun Orders(@Header("Authorization") Authorization:String,
               @Header("lang") lang:String,
               @Query("status") status:String):Call<OrderResponse>
    @POST("user-contracte")
    fun UserContarct(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
    @Query("order_id") order_id:Int,
    @Query("user_phone") user_phone:String,
    @Query("user_name") user_name:String,
    @Query("id_number") id_number:String,
    @Query("user_date") user_date:String):Call<TermsResponse>

    @POST("provider-contracte")
    fun ProviderContract(@Header("Authorization") Authorization:String,
                         @Header("lang") lang:String,
                         @Query("order_id") order_id:Int,
                         @Query("provider_phone") user_phone:String,
                         @Query("provider_name") user_name:String,
                         @Query("provider_id_number") id_number:String,
                         @Query("provider_date") user_date:String):Call<TermsResponse>
}