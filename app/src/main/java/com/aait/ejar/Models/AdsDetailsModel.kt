package com.aait.ejar.Models

import java.io.Serializable

class AdsDetailsModel:Serializable {
    var id:Int?=null
    var category_id:Int?=null
    var category:String?=null
    var property_space:String?=null
    var neighborhood:String?=null
    var neighborhood_ar:String?=null
    var neighborhood_en:String?=null
    var number_rooms:String?=null
    var number_roles:String?=null
    var annual_amount:String?=null
    var monthly_amount:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var images:ArrayList<ImagesModel>?=null
}