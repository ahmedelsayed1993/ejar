package com.aait.ejar.Models

import java.io.Serializable

class CategoryModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }
}