package com.aait.ejar.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var property_id:Int?=null
    var avatar:String?=null
    var category:String?=null
    var created:String?=null
}