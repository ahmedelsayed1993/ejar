package com.aait.ejar.Models

import java.io.Serializable

class SocialModel:Serializable {
    var name:String?=null
    var link:String?=null
    var id:Int?=null
}