package com.aait.ejar.Models

import java.io.Serializable

class ChatResponse:BaseResponse(),Serializable {
    var paginate:PaginationModel?=null
    var data:ArrayList<ChatModel>?=null
    var show:String?=null
    var order_id:Int?=null
   // var user_data:user?=null
}