package com.aait.ejar.Models

import java.io.Serializable

class PropertiesModel:Serializable {
    var id:Int?=null
    var lat:String?=null
    var lng:String?=null
    var distance:Int?=null
    var monthly:String?=null
}