package com.aait.ejar.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var sliders:ArrayList<String>?=null
    var categories:ArrayList<CategoryModel>?=null
    var properties:ArrayList<PropertiesModel>?=null
}