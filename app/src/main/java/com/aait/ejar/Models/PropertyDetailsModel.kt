package com.aait.ejar.Models

import java.io.Serializable

class PropertyDetailsModel:Serializable {
    var id:Int?=null
    var receiver:Int?=null
    var username:String?=null
    var avatar:String?=null
    var category:String?=null
    var property_space:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var neighborhood:String?=null
    var number_rooms:String?=null
    var number_roles:String?=null
    var annual_amount:String?=null
    var monthly_amount:String?=null
    var iamges:ArrayList<String>?=null
    var check_provider:String?=null
}