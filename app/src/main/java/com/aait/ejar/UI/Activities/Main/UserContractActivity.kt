package com.aait.ejar.UI.Activities.Main

import android.app.DatePickerDialog
import android.content.Intent
import android.widget.*
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Models.TermsResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class UserContractActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_user_contract
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var id_num:EditText
    lateinit var date:TextView

    lateinit var send:Button
    var d = ""
    var order_id = 0
    override fun initializeComponents() {
        order_id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        id_num = findViewById(R.id.id_num)
        date = findViewById(R.id.date)

        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.contract_info)

        date.setOnClickListener {
            val c = Calendar.getInstance()
            val Year = c.get(Calendar.YEAR)
            val Month = c.get(Calendar.MONTH)
            val Day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                date.text = ("" + dayOfMonth + "/" + m + "/" + year)

            }, Year, Month, Day)

            dpd.show()
        }
        send.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(id_num,getString(R.string.enter_id_number))||
                    CommonUtil.checkLength(id_num,getString(R.string.id_number),10)||
                    CommonUtil.checkTextError(date,getString(R.string.date_of_birth))){
                return@setOnClickListener
            }else{
                Send()
            }
        }
    }

    fun Send(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UserContarct("Bearer"+user.userData.token,lang.appLanguage
        ,order_id,phone.text.toString(),name.text.toString(),id_num.text.toString(),date.text.toString())?.enqueue(object : Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@UserContractActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}