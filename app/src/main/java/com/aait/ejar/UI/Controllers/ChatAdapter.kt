package com.aait.ejar.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


import com.aait.ejar.Models.ChatModel
import com.aait.ejar.R

import com.bumptech.glide.Glide

import de.hdodenhof.circleimageview.CircleImageView

class ChatAdapter(
    internal var context: Context,
    internal var messageModels: List<ChatModel>,
    internal var id: Int
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    internal var paramsMsg: LinearLayout.LayoutParams? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View
        return if (viewType == 0) {
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.card_chat_send,
                    parent,
                    false
                )
            )
        } else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.card_chat_receive,
                    parent,
                    false
                )
            )
    }

    override fun getItemViewType(position: Int): Int {

        return if (messageModels[position].receiver_id == id) {

            1
        } else {

            0
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (messageModels[position].messageType.equals("text")) {

            holder.msg.text = messageModels[position].message
            holder.date.text = messageModels[position].created
            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
        }
//        if (messageModels[position].messageType.equals("file")){
//            holder.chat.visibility = View.GONE
//            holder.image_lay.visibility = View.VISIBLE
//            holder.time.text = messageModels[position].created
//            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
//            Glide.with(context).load(messageModels[position].message).into(holder.image)
//        }




    }

    override fun getItemCount(): Int {
        return messageModels.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var msg: TextView
        internal var date: TextView
        internal var sender: CircleImageView




        init {
            msg = itemView.findViewById(R.id.tv_name)
            date = itemView.findViewById(R.id.tv_date)
            sender = itemView.findViewById(R.id.sender)




        }
    }
}
