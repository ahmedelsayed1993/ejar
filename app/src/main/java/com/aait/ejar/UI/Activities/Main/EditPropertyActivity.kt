package com.aait.ejar.UI.Activities.Main

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Listeners.OnItemClickListener
import com.aait.ejar.Models.*
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.LocationActivity
import com.aait.ejar.UI.Controllers.ImageAdapter
import com.aait.ejar.UI.Controllers.ListAdapter
import com.aait.ejar.Utils.CommonUtil
import com.aait.ejar.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditPropertyActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_edit_property
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var type: TextView
    lateinit var area: EditText
    lateinit var location: TextView
    lateinit var Neighborhood: EditText
    lateinit var roles: EditText
    lateinit var rooms: EditText
    lateinit var monthly: EditText
    lateinit var annual: EditText
    lateinit var types: RecyclerView
    lateinit var photos: TextView
    lateinit var add: Button
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(10)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    var paths = ArrayList<String>()
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var listAdapter: ListAdapter
    var cats = ArrayList<CategoryModel>()
    lateinit var categoryModel: CategoryModel
    lateinit var images:RecyclerView
    lateinit var imageAdapter: ImageAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var Images = ArrayList<ImagesModel>()
    var ids = ArrayList<Int>()
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        type = findViewById(R.id.type)
        area = findViewById(R.id.area)
        location = findViewById(R.id.location)
        Neighborhood = findViewById(R.id.Neighborhood)
        roles = findViewById(R.id.roles)
        rooms = findViewById(R.id.rooms)
        monthly = findViewById(R.id.monthly)
        annual = findViewById(R.id.annual)
        types = findViewById(R.id.types)
        add = findViewById(R.id.add)
        photos = findViewById(R.id.photos)
        images = findViewById(R.id.images)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        imageAdapter = ImageAdapter(mContext,Images,R.layout.recycler_image)
        imageAdapter.setOnItemClickListener(this)
        images.layoutManager = linearLayoutManager1
        images.adapter = imageAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.edit)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        listAdapter = ListAdapter(mContext,cats,R.layout.recycle_list)
        listAdapter.setOnItemClickListener(this)
        types.layoutManager = linearLayoutManager
        types.adapter = listAdapter
        photos.setOnClickListener {  if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                    )
                }
            } else {
                Pix.start(this, options1)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options1)
        } }
           getData()
        location.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        type.setOnClickListener {
            getCategories()
        }
        add.setOnClickListener {
            if (CommonUtil.checkTextError(type,getString(R.string.enter_Type_of_property))||
                    CommonUtil.checkEditError(area,getString(R.string.enter_The_area_of_the_property))||
                    CommonUtil.checkTextError(location,getString(R.string.enter_The_location_of_the_property))||
                    CommonUtil.checkEditError(Neighborhood,getString(R.string.enter_Neighborhood))||
                    CommonUtil.checkEditError(roles,getString(R.string.enter_Number_of_roles))||
                    CommonUtil.checkEditError(rooms,getString(R.string.enter_The_number_of_rooms))||
                    CommonUtil.checkEditError(annual,getString(R.string.annual))||
                    CommonUtil.checkEditError(monthly,getString(R.string.Monthly))){
                return@setOnClickListener
            }else {
                if (ids.isEmpty()) {
                    if (paths.isEmpty()) {
                        AddProduct(null)
                    } else {
                        AddProduct(paths, null)
                    }
                } else {
                    if (paths.isEmpty()) {
                        AddProduct(ids.joinToString(separator = ",", postfix = "", prefix = ""))
                    } else {
                        AddProduct(paths, ids.joinToString(separator = ",", postfix = "", prefix = ""))
                    }
                }
            }
        }
    }
    fun getCategories(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Categories(lang.appLanguage)?.enqueue(object : Callback<ListResponse> {
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                types.visibility = View.GONE
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        types.visibility = View.VISIBLE
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        types.visibility = View.GONE
                    }
                }
            }

        })
    }
fun getData(){
    showProgressDialog(getString(R.string.please_wait))
    Client.getClient()?.create(Service::class.java)?.editProperty(lang.appLanguage,"Bearer"+user.userData.token,id,null
    ,null,null,null,null,null,null,null
    ,null,null,null,null)?.enqueue(object :Callback<AdsDetailsResponse>{
        override fun onFailure(call: Call<AdsDetailsResponse>, t: Throwable) {
            CommonUtil.handleException(mContext,t)
            t.printStackTrace()
            hideProgressDialog()
        }

        override fun onResponse(call: Call<AdsDetailsResponse>, response: Response<AdsDetailsResponse>) {
            hideProgressDialog()
            if (response.isSuccessful){
                if (response.body()?.value.equals("1")){
                    categoryModel = CategoryModel(response.body()?.data?.category_id,response.body()?.data?.category)
                    type.text = categoryModel.name
                    area.setText(response.body()?.data?.property_space)
                    lat = response.body()?.data?.lat!!
                    lng = response.body()?.data?.lng!!
                    result = response.body()?.data?.address!!
                    imageAdapter.updateAll(response.body()?.data?.images!!)
                    Neighborhood.setText(response.body()?.data?.neighborhood)
                    roles.setText(response.body()?.data?.number_roles)
                    rooms.setText(response.body()?.data?.number_rooms)
                    location.text = response.body()?.data?.address
                    annual.setText(response.body()?.data?.annual_amount)
                    monthly.setText(response.body()?.data?.monthly_amount)
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }
        }

    })
}
    fun AddProduct(imges:ArrayList<String>,ids:String?){
        showProgressDialog(getString(R.string.please_wait))
        var imgs = ArrayList<MultipartBody.Part>()
        for (i in 0..imges.size-1){
            var filePart: MultipartBody.Part? = null
            val ImageFile = File(imges.get(i))
            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
            imgs.add(filePart)
        }
        Client.getClient()?.create(Service::class.java)?.editProperty(lang.appLanguage,"Bearer"+user.userData.token,id
                ,categoryModel.id!!,area.text.toString(),location.text.toString(),lat,lng,Neighborhood.text.toString(),Neighborhood.text.toString()
                ,rooms.text.toString(),roles.text.toString(),annual.text.toString(),monthly.text.toString(),ids,imgs)?.enqueue(object : Callback<AdsDetailsResponse> {
            override fun onFailure(call: Call<AdsDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<AdsDetailsResponse>, response: Response<AdsDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        onBackPressed()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun AddProduct(ids:String?){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.editProperty(lang.appLanguage,"Bearer"+user.userData.token,id
                ,categoryModel.id!!,area.text.toString(),location.text.toString(),lat,lng,Neighborhood.text.toString(),Neighborhood.text.toString()
                ,rooms.text.toString(),roles.text.toString(),annual.text.toString(),monthly.text.toString(),ids)?.enqueue(object : Callback<AdsDetailsResponse> {
            override fun onFailure(call: Call<AdsDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<AdsDetailsResponse>, response: Response<AdsDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        onBackPressed()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            types.visibility = View.GONE
            categoryModel = cats.get(position)
            type.text = categoryModel.name
        }else if (view.id == R.id.delete){
            ids.add(Images.get(position).id!!)
            Images.remove(Images.get(position))
            imageAdapter.notifyDataSetChanged()
            Log.e("ids",ids.joinToString(separator = "," , postfix = "",prefix = ""))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 200) {
            if (resultCode == 0) {

            } else {
                returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                // ImageBasePath = returnValue!![0]
                for (i in 0..returnValue1!!.size - 1) {
                    paths.add(returnValue1!![i])
                }
                if (paths.size != 0) {
                    photos.text = getString(R.string.images_attached)

                }
                Log.e("pathsss", Gson().toJson(returnValue1))


//                if (ImageBasePath != null) {
//                    // upLoad(ImageBasePath!!)
//                    image.text = getString(R.string.attach_image)
//                }
            }

        }else{
            if (resultCode == 1) {
                if (data?.getStringExtra("result") != null) {
                    result = data?.getStringExtra("result").toString()
                    lat = data?.getStringExtra("lat").toString()
                    lng = data?.getStringExtra("lng").toString()
                    location.text = result
                } else {
                    result = ""
                    lat = ""
                    lng = ""
                    location.text = ""
                }
            }
        }
    }
}