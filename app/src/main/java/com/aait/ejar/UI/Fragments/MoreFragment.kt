package com.aait.ejar.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import com.aait.ejar.Base.BaseFragment
import com.aait.ejar.Models.NotifyResponse
import com.aait.ejar.Models.TermsResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.AppInfo.AboutAppActivity
import com.aait.ejar.UI.Activities.AppInfo.AppLangActivity
import com.aait.ejar.UI.Activities.AppInfo.CallUsActivity
import com.aait.ejar.UI.Activities.AppInfo.TermsActivity
import com.aait.ejar.UI.Activities.Auth.LoginActivity
import com.aait.ejar.UI.Activities.Auth.ProfileActivity
import com.aait.ejar.UI.Activities.Main.NotificationActivity
import com.aait.ejar.UI.Activities.SplashActivity
import com.aait.ejar.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoreFragment :BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_more
    companion object {
        fun newInstance(): MoreFragment {
            val args = Bundle()
            val fragment = MoreFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var notification:ImageView
    lateinit var profile:LinearLayout
    lateinit var share_app:LinearLayout
    lateinit var notifi:Switch
    lateinit var language:LinearLayout
    lateinit var about_app:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var contact_us:LinearLayout
    lateinit var logout:LinearLayout
    lateinit var text:TextView
    lateinit var title:TextView

    override fun initializeComponents(view: View) {
        notification = view.findViewById(R.id.notification)
        profile = view.findViewById(R.id.profile)
        share_app = view.findViewById(R.id.share_app)
        notifi = view.findViewById(R.id.notifi)
        language = view.findViewById(R.id.language)
        about_app = view.findViewById(R.id.about_app)
        terms = view.findViewById(R.id.terms)
        contact_us = view.findViewById(R.id.contact_us)
        logout = view.findViewById(R.id.logout)
        text = view.findViewById(R.id.text)
        title = view.findViewById(R.id.title)
        title.text = getString(R.string.settings)
        if(user.loginStatus!!){
            text.text = getString(R.string.logout)
        }else{
            text.text = getString(R.string.sign_in)
        }
        notification.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity, NotificationActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        if (user.loginStatus!!){
            notify(null)
        }else{

        }
        language.setOnClickListener { startActivity(Intent(activity,AppLangActivity::class.java)) }
        notifi.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (isChecked) {
                // theme.appTheme =
                if (user.loginStatus!!){
                    notify(1)
                }

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }else{
                //  theme.appTheme =
                if (user.loginStatus!!){
                    notify(0)
                }

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }
        })

        share_app.setOnClickListener { CommonUtil.ShareApp(mContext!!) }
        terms.setOnClickListener { startActivity(Intent(activity,TermsActivity::class.java)) }
        about_app.setOnClickListener { startActivity(Intent(activity,AboutAppActivity::class.java)) }
        profile.setOnClickListener { if (user.loginStatus!!){startActivity(Intent(activity,ProfileActivity::class.java)) }
        else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }}
        contact_us.setOnClickListener { startActivity(Intent(activity,CallUsActivity::class.java)) }
        logout.setOnClickListener {if(user.loginStatus!!){ logout() }
        else{
            startActivity(Intent(activity, LoginActivity::class.java))
        }}

    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,"android",lang.appLanguage)?.enqueue(object :
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()

                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                    }else if(response.body()?.value.equals("401")){
                        user.loginStatus=false
                        user.Logout()
                        startActivity(Intent(activity, LoginActivity::class.java))
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)

                    }
                }
            }
        })
    }
    fun notify(swit:Int?){
        Client.getClient()?.create(Service::class.java)?.Switch("Bearer"+user.userData.token,swit)?.enqueue(object :Callback<NotifyResponse>{
            override fun onFailure(call: Call<NotifyResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<NotifyResponse>, response: Response<NotifyResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            notifi.isChecked = true
                        }else{
                            notifi.isChecked = false
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}