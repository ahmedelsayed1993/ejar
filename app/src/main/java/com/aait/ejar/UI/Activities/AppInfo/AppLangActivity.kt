package com.aait.ejar.UI.Activities.AppInfo

import android.content.Intent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.R

import com.aait.ejar.UI.Activities.SplashActivity

class AppLangActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_app_lang
    lateinit var arabic:LinearLayout
    lateinit var english:LinearLayout

    lateinit var back:ImageView
    lateinit var title:TextView


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)

        back.setOnClickListener { onBackPressed()
        finish()}

        title.text = getString(R.string.language)

       arabic.setOnClickListener {
           lang.appLanguage = "ar"
           finishAffinity()
           startActivity(Intent(this, SplashActivity::class.java))
       }
        english.setOnClickListener {
            lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
    }
}