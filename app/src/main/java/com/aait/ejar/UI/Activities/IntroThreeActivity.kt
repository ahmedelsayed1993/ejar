package com.aait.ejar.UI.Activities

import android.content.Intent
import android.widget.TextView
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Models.TermsResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Auth.LoginActivity
import com.aait.ejar.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IntroThreeActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_three
    lateinit var title: TextView
    lateinit var desc: TextView

    lateinit var next: TextView
    override fun initializeComponents() {
        title = findViewById(R.id.title)
        desc = findViewById(R.id.desc)
        next = findViewById(R.id.next)
        next.setOnClickListener { startActivity(Intent(this, LoginActivity::class.java))
            finish()}
        title.text = getString(R.string.app_name)
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(lang.appLanguage)?.enqueue(object:
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        desc.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}