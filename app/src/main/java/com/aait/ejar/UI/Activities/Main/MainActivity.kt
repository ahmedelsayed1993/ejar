package com.aait.ejar.UI.Activities.Main

import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.R
import com.aait.ejar.UI.Fragments.*
import com.aait.ejar.Utils.CommonUtil

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main

    lateinit var orders:LinearLayout
    lateinit var order_image:ImageView
    lateinit var ads:LinearLayout
    lateinit var ads_image:ImageView
    lateinit var home:LinearLayout
    lateinit var home_image:ImageView
    lateinit var chats:LinearLayout
    lateinit var chats_image:ImageView
    lateinit var more:LinearLayout
    lateinit var more_image:ImageView
    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var chatsFragment: ChatsFragment
    internal lateinit var ordersFragment: OrdersFragment
    internal lateinit var adsFragment: AdsFragment
    internal lateinit var moreFragment: MoreFragment
    override fun initializeComponents() {
        orders = findViewById(R.id.orders)
        order_image = findViewById(R.id.order_image)
        ads = findViewById(R.id.ads)
        ads_image = findViewById(R.id.ads_image)
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        chats = findViewById(R.id.chats)
        chats_image = findViewById(R.id.chats_image)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        homeFragment = HomeFragment.newInstance()
        chatsFragment = ChatsFragment.newInstance()
        ordersFragment = OrdersFragment.newInstance()
        adsFragment = AdsFragment.newInstance()
        moreFragment = MoreFragment.newInstance()
        fragmentManager = supportFragmentManager
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,adsFragment)
        transaction!!.add(R.id.home_fragment_container,homeFragment)
        transaction!!.add(R.id.home_fragment_container,chatsFragment)
        transaction!!.add(R.id.home_fragment_container,moreFragment)
        transaction!!.commit()
        showhome()
        home.setOnClickListener { showhome() }
        orders.setOnClickListener { showorders() }
        ads.setOnClickListener { showads() }
        chats.setOnClickListener { showchats() }
        more.setOnClickListener { showmore() }
    }

    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.backhome)
        more_image.setImageResource(R.mipmap.mo)
        order_image.setImageResource(R.mipmap.interview)
        ads_image.setImageResource(R.mipmap.promotion)
        chats_image.setImageResource(R.mipmap.chat)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }
    fun showorders(){
        selected = 1
        if (user.loginStatus!!) {
            home_image.setImageResource(R.mipmap.backhome)
            more_image.setImageResource(R.mipmap.mo)
            order_image.setImageResource(R.mipmap.intervie)
            ads_image.setImageResource(R.mipmap.promotion)
            chats_image.setImageResource(R.mipmap.chat)
            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, ordersFragment)
            transaction!!.commit()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
    }
    fun showads(){
        selected = 1
        if (user.loginStatus!!) {
            home_image.setImageResource(R.mipmap.backhome)
            more_image.setImageResource(R.mipmap.mo)
            order_image.setImageResource(R.mipmap.interview)
            ads_image.setImageResource(R.mipmap.promoti)
            chats_image.setImageResource(R.mipmap.chat)
            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, adsFragment)
            transaction!!.commit()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
    }
    fun showchats(){
        selected = 1
        if (user.loginStatus!!) {
            home_image.setImageResource(R.mipmap.backhome)
            more_image.setImageResource(R.mipmap.mo)
            order_image.setImageResource(R.mipmap.interview)
            ads_image.setImageResource(R.mipmap.promotion)
            chats_image.setImageResource(R.mipmap.cha)
            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, chatsFragment)
            transaction!!.commit()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
    }
    fun showmore(){
        selected = 1
        home_image.setImageResource(R.mipmap.backhome)
        more_image.setImageResource(R.mipmap.mor)
        order_image.setImageResource(R.mipmap.interview)
        ads_image.setImageResource(R.mipmap.promotion)
        chats_image.setImageResource(R.mipmap.chat)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, moreFragment)
        transaction!!.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}