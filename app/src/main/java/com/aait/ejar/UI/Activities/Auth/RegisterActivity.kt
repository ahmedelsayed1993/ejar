package com.aait.ejar.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.widget.*
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Models.UserResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.AppInfo.TermsActivity
import com.aait.ejar.UI.Activities.LocationActivity
import com.aait.ejar.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var register:Button
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var email:EditText
    lateinit var locatrion:TextView
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm_password:EditText
    lateinit var view1:ImageView
    lateinit var terms:CheckBox
    lateinit var login:LinearLayout
    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        locatrion = findViewById(R.id.location)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        terms = findViewById(R.id.terms)
        confirm_password = findViewById(R.id.confirm_password)
        view1 = findViewById(R.id.view1)
        login = findViewById(R.id.login)
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view1.setOnClickListener { if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        locatrion.setOnClickListener {
            startActivityForResult(Intent(this,LocationActivity::class.java),1)
        }
        register.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.enter_email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(locatrion,getString(R.string.enter_location))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
            return@setOnClickListener
        } else{
             if (!password.text.toString().equals(confirm_password.text.toString())) {
                CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
            } else {
                if (!terms.isChecked) {
                    CommonUtil.makeToast(mContext, getString(R.string.read_accept))
                } else {

                    Register()
                }

            }
        }
        }

    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(name.text.toString(),phone.text.toString(),email.text.toString(),locatrion.text.toString()
                ,lat,lng,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivateCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                locatrion.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                locatrion.text = ""
            }
        }
    }
}