package com.aait.ejar.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ejar.Base.ParentRecyclerAdapter
import com.aait.ejar.Base.ParentRecyclerViewHolder
import com.aait.ejar.Models.ImagesModel
import com.aait.ejar.R
import com.bumptech.glide.Glide

class AdsAdapter (context: Context, data: MutableList<ImagesModel>, layoutId: Int) :
        ParentRecyclerAdapter<ImagesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)

        Glide.with(mcontext).asBitmap().load(questionModel.image).into(viewHolder.image)
        viewHolder.lay.visibility = View.GONE
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.image.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
          if (viewHolder.lay.visibility==View.GONE){
              viewHolder.lay.visibility = View.VISIBLE
          }else{
              viewHolder.lay.visibility = View.GONE
          }
        })
        viewHolder.edit.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.delete.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.show.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {
        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var more = itemView.findViewById<ImageView>(R.id.more)
        internal var lay = itemView.findViewById<LinearLayout>(R.id.lay)
        internal var edit = itemView.findViewById<TextView>(R.id.edit)
        internal var show = itemView.findViewById<TextView>(R.id.show)
        internal var delete = itemView.findViewById<TextView>(R.id.delete)
    }
}