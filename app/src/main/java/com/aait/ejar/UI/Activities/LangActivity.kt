package com.aait.ejar.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.R

class LangActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_choose_lang
       lateinit var arabic:Button
    lateinit var english:Button
    override fun initializeComponents() {
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        arabic.setOnClickListener {
            lang.appLanguage = "ar"
            startActivity(Intent(this,IntroOneActivity::class.java))
        }
        english.setOnClickListener {
            lang.appLanguage = "en"
            startActivity(Intent(this,IntroOneActivity::class.java))
        }

    }
}