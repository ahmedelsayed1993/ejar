package com.aait.ejar.UI.Controllers

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.ejar.R
import com.aait.ejar.UI.Fragments.ActiveAds
import com.aait.ejar.UI.Fragments.ExpireAds


class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            ActiveAds()
        } else {
            ExpireAds()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.Constant_ads)
        } else {
            context.getString(R.string.Expired_ads)
        }
    }
}
