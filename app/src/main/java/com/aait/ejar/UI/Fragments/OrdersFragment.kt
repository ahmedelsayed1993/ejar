package com.aait.ejar.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.ejar.Base.BaseFragment
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Main.NotificationActivity
import com.aait.ejar.UI.Controllers.SubSribeTapAdapter
import com.aait.ejar.Utils.CommonUtil
import com.google.android.material.tabs.TabLayout

class OrdersFragment :BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    companion object {
        fun newInstance(): OrdersFragment {
            val args = Bundle()
            val fragment = OrdersFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var notification: ImageView
    lateinit var title: TextView
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubSribeTapAdapter? = null
    override fun initializeComponents(view: View) {
        notification = view.findViewById(R.id.notification)
        title = view.findViewById(R.id.title)
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = SubSribeTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
        title.text = getString(R.string.requests)
        notification.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity, NotificationActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
    }
}