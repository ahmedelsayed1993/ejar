package com.aait.ejar.UI.Activities.Main

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Listeners.OnItemClickListener
import com.aait.ejar.Models.PropertyDetailsResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Controllers.ImagesAdapter
import com.aait.ejar.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity : ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_property_details

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var image: ImageView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var type: TextView
    lateinit var area: TextView
    lateinit var location: TextView
    lateinit var Neighborhood: TextView
    lateinit var roles: TextView
    lateinit var rooms: TextView
    lateinit var monthly: TextView
    lateinit var annual: TextView
    lateinit var images: RecyclerView
    lateinit var chat: Button
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var imagesAdapter: ImagesAdapter
    var Images = ArrayList<String>()
    var id = 0
    var reciver = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address= findViewById(R.id.address)
        type = findViewById(R.id.type)
        area = findViewById(R.id.area)
        location = findViewById(R.id.location)
        Neighborhood = findViewById(R.id.Neighborhood)
        roles = findViewById(R.id.roles)
        rooms = findViewById(R.id.rooms)
        monthly = findViewById(R.id.monthly)
        annual = findViewById(R.id.annual)
        images = findViewById(R.id.images)
        chat = findViewById(R.id.chat)
        gridLayoutManager = GridLayoutManager(mContext,3)
        imagesAdapter = ImagesAdapter(mContext,Images, R.layout.recycle_image)
        imagesAdapter.setOnItemClickListener(this)
        images.layoutManager = gridLayoutManager
        images.adapter = imagesAdapter
        if (user.loginStatus!!) {
            getData(user.userData.token)
        }else{
            getData(null)
        }
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Property_information)
        chat.visibility = View.GONE

    }

    fun getData(token:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Details(lang.appLanguage,token,id)?.enqueue(object : Callback<PropertyDetailsResponse> {
            override fun onFailure(call: Call<PropertyDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<PropertyDetailsResponse>, response: Response<PropertyDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        reciver = response.body()?.data?.receiver!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.text = response?.body()?.data?.username
                        address.text = response?.body()?.data?.address
                        type.text = response?.body()?.data?.category
                        area.text = response.body()?.data?.property_space
                        location.text = response?.body()?.data?.address
                        Neighborhood.text = response?.body()?.data?.neighborhood
                        rooms.text = response?.body()?.data?.number_rooms
                        roles.text = response?.body()?.data?.number_roles
                        annual.text = getString(R.string.annual)+":"+response?.body()?.data?.annual_amount+getString(R.string.Rial)
                        monthly.text = getString(R.string.Monthly)+":"+response?.body()?.data?.monthly_amount+getString(R.string.Rial)
                        imagesAdapter.updateAll(response.body()?.data?.iamges!!)
                        if (response.body()?.data?.check_provider.equals("provider")){
                            chat.visibility = View.GONE
                        }else{
                            chat.visibility = View.GONE
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent  = Intent(this, ImagesActivity::class.java)
        intent.putExtra("link",Images)
        startActivity(intent)
    }

}