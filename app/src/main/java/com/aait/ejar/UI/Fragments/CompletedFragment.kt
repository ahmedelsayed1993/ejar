package com.aait.ejar.UI.Fragments

import android.content.Intent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ejar.Base.BaseFragment
import com.aait.ejar.Listeners.OnItemClickListener
import com.aait.ejar.Models.OrderModel
import com.aait.ejar.Models.OrderResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Main.OrderDetailsActivity
import com.aait.ejar.UI.Controllers.OrderAdapter
import com.aait.ejar.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompletedFragment:BaseFragment() , OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.app_recycle
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var adsAdapter: OrderAdapter
    var ads = ArrayList<OrderModel>()
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL,false)
        adsAdapter = OrderAdapter(mContext!!,ads,R.layout.recycle_order)
        adsAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = adsAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
           if (user.loginStatus!!) {
               getData()
           }else{
               CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
           }


        }
        if (user.loginStatus!!){
        getData()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders("Bearer"+user.userData.token,lang.appLanguage,"completed")?.enqueue(object:
                Callback<OrderResponse> {
            override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<OrderResponse>,
                    response: Response<OrderResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            adsAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, OrderDetailsActivity::class.java)

        intent.putExtra("id",ads.get(position).property_id)

        startActivity(intent)
    }
}