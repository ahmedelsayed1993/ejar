package com.aait.ejar.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.ejar.Base.ParentRecyclerAdapter
import com.aait.ejar.Base.ParentRecyclerViewHolder
import com.aait.ejar.Models.ImagesModel
import com.aait.ejar.R
import com.bumptech.glide.Glide

class ImageAdapter (context: Context, data: MutableList<ImagesModel>, layoutId: Int) :
        ParentRecyclerAdapter<ImagesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        Glide.with(mcontext).asBitmap().load(questionModel.image).into(viewHolder.image)
        viewHolder.delete.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var delete=itemView.findViewById<ImageView>(R.id.delete)
        internal var image=itemView.findViewById<ImageView>(R.id.image)

    }
}