package com.aait.ejar.UI.Fragments

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ejar.Base.BaseFragment
import com.aait.ejar.GPS.GPSTracker
import com.aait.ejar.GPS.GpsTrakerListener
import com.aait.ejar.Listeners.OnItemClickListener
import com.aait.ejar.Models.CategoryModel
import com.aait.ejar.Models.HomeResponse
import com.aait.ejar.Models.PropertiesModel
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Main.AddPropertyActivity
import com.aait.ejar.UI.Activities.Main.NotificationActivity
import com.aait.ejar.UI.Activities.Main.PropertyDetailsActivity
import com.aait.ejar.UI.Controllers.CategoryAdapter
import com.aait.ejar.UI.Controllers.SlidersAdapter
import com.aait.ejar.Utils.CommonUtil
import com.aait.ejar.Utils.DialogUtil
import com.aait.ejar.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import de.hdodenhof.circleimageview.CircleImageView
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.Float
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment :BaseFragment(),OnItemClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var notification:ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var indicator:CircleIndicator
    lateinit var cats:RecyclerView
    lateinit var map:MapView
    lateinit var search:EditText
    lateinit var icon:ImageView
    lateinit var add:LinearLayout
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var categoryAdapter: CategoryAdapter
    var categoryModels = ArrayList<CategoryModel>()
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var goTOMyLocation:ImageView
    var mLang = ""
    var mLat = ""
    var result = ""
     var cat:Int?=null

    var canMove = true

    internal lateinit var markerOptions: MarkerOptions
    internal var hmap = HashMap<Marker, PropertiesModel>()
    override fun initializeComponents(view: View) {
        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        notification = view.findViewById(R.id.notification)
        viewPager = view.findViewById(R.id.viewPager)
        indicator = view.findViewById(R.id.indicator)
        goTOMyLocation = view.findViewById(R.id.goTOMyLocation)
        cats = view.findViewById(R.id.cats)
        icon = view.findViewById(R.id.icon)
        map = view.findViewById(R.id.map)
        search = view.findViewById(R.id.search)
        add = view.findViewById(R.id.add)
        if (user.loginStatus!!){
            Glide.with(mContext!!).asBitmap().load(user.userData.avatar).into(image)
            name.text = getString(R.string.welcome)+"("+user.userData.name+")"
        }else{

        }
        icon.setOnClickListener {
            if (search.text.equals("")){

            }else{
                getLocationWithPermission(null,search.text.toString())
            }
        }
        add.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity,AddPropertyActivity::class.java))

            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        notification.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity, NotificationActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)
        categoryAdapter = CategoryAdapter(mContext!!,categoryModels,R.layout.recycle_cats)
        categoryAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = categoryAdapter
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        hideKeyboard()
        getLocationWithPermission(null,search.text.toString())

       goTOMyLocation.setOnClickListener {
           getLocationWithPermission(null,search.text.toString())
       }
    }

//    override fun onResume() {
//        super.onResume()
//        getLocationWithPermission(null,null)
//    }
private fun hideKeyboard() {
    val view = activity?.currentFocus
    if (view != null) {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    // else {
    activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}

    override fun onItemClick(view: View, position: Int) {
        cat = categoryModels.get(position).id
        getLocationWithPermission(cat,search.text.toString())
    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        hideProgressDialog()
        googleMap!!.setOnMarkerClickListener(this)
        getLocationWithPermission(null,search.text.toString())
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(mLat.toDouble(),mLang.toDouble()), 9f))
        googleMap!!.setOnCameraIdleListener {
            Log.e("jjjj","uuuuuuu")
             getData(googleMap!!.cameraPosition.target.latitude.toString(),googleMap!!.cameraPosition.target.longitude.toString(),cat,search.text.toString())
        }

      //  googleMap!!.setOnCameraMoveListener { getData(googleMap!!.cameraPosition.target.latitude.toString(),googleMap!!.cameraPosition.target.longitude.toString(),null,null) }

    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        if (p0!!.getTitle() != "موقعي") {
            if (hmap.containsKey(p0)) {
                val myShopModel = hmap.get(p0)

                val intent = Intent(activity, PropertyDetailsActivity::class.java)

                intent.putExtra("id", myShopModel!!.id)

                startActivity(intent)
            }
        }



        return false
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission(cat:Int?,text: String?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat,text)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat,text)
        }

    }

    internal fun getCurrentLocation(cat:Int?,text:String?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker1(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString(),cat,text)

                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }
               // putMapMarker1(gps.latitude,gps.longitude)

                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun putMapMarker1(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
//        myMarker = googleMap!!.addMarker(
//                MarkerOptions()
//                        .position(latLng)
//                        .title("موقعي")
//                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.backlocation))
//        )
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))
        // kkjgj

    }
    fun getData(lat:String,lng:String,cat:Int?,text:String?){
        // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Home(lang.appLanguage,lat,lng,cat,text)?.enqueue(object:
                Callback<HomeResponse> {
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<HomeResponse>,
                    response: Response<HomeResponse>
            ) {
                hideProgressDialog()

                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.sliders!!)
                        categoryAdapter.updateAll(response.body()?.categories!!)
                        if (response.body()!!.properties?.isEmpty()!!) {
                            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_no))
                            googleMap!!.clear()

                        } else {
                            googleMap!!.clear()

                            for (i in 0..response.body()?.properties?.size!!-1){
                                addShop(response.body()?.properties?.get(i)!!)
                            }

                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun initSliderAds(list: java.util.ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility=View.GONE
            indicator.visibility = View.GONE
        }
        else{
            viewPager.visibility=View.VISIBLE
            indicator.visibility = View.VISIBLE
            viewPager.adapter= SlidersAdapter(mContext!!,list)
            indicator.setViewPager(viewPager)
        }
    }
    internal fun addShop(shopModel: PropertiesModel) {
        markerOptions = MarkerOptions().position(
                LatLng(Float.parseFloat(shopModel.lat!!).toDouble(), Float.parseFloat(shopModel.lng!!).toDouble()))
                .title("")
                .icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(shopModel.distance.toString()+" KM | "+shopModel.monthly+" "+getString(R.string.Rial))))
        myMarker = googleMap!!.addMarker(markerOptions)

//        googleMap!!.animateCamera(
//                CameraUpdateFactory.newLatLngZoom(LatLng(java.lang.Double.parseDouble(shopModel.lat!!), java.lang.Double.parseDouble(shopModel.lng!!)), 10f)
//        )
        hmap[myMarker] = shopModel

    }
    private fun createStoreMarker(text:String): Bitmap? {
        val markerLayout: View = layoutInflater.inflate(R.layout.custom_marker, null)
        val markerImage = markerLayout.findViewById<View>(R.id.marker_image) as ImageView
        val markerRating = markerLayout.findViewById<View>(R.id.marker_text) as TextView
        markerImage.setImageResource(R.mipmap.map)
        markerRating.setText(text)
        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        markerLayout.layout(0, 0, markerLayout.measuredWidth, markerLayout.measuredHeight)
        val bitmap: Bitmap = Bitmap.createBitmap(markerLayout.measuredWidth, markerLayout.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        markerLayout.draw(canvas)
        return bitmap
    }

//    override fun onCameraIdle() {
//        val target = LatLng(googleMap!!.cameraPosition.target.latitude,googleMap!!.cameraPosition.target.longitude)
//        getData(target.latitude.toString(),target.longitude.toString(),cat,search.text.toString())
//
//    }
}