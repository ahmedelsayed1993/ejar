package com.aait.ejar.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.ejar.R
import com.aait.ejar.UI.Fragments.*

class SubSribeTapAdapter (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            PendingFragment()
        } else if (position == 1){
            CurrentFragment()
        }else{
            CompletedFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.Pending_requests)
        } else if (position == 1) {
            context.getString(R.string.Active_requests)
        }else{
            context.getString(R.string.Completed_requests)
        }
    }
}
