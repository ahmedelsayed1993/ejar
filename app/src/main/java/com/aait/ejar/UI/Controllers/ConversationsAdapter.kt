package com.aait.ejar.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.ejar.Base.ParentRecyclerAdapter
import com.aait.ejar.Base.ParentRecyclerViewHolder
import com.aait.ejar.Models.ChatsModel
import com.aait.ejar.R
import com.bumptech.glide.Glide

class ConversationsAdapter (context: Context, data: MutableList<ChatsModel>, layoutId: Int) :
    ParentRecyclerAdapter<ChatsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val chatsModel = data.get(position)
        viewHolder.name!!.setText(chatsModel.username)

            viewHolder.message!!.text = chatsModel.message!!

        //  Glide.with(mcontext).load(listModel.image!!).into(viewHolder.photo)
        Glide.with(mcontext).load(chatsModel.avatar).into(viewHolder.image)

            viewHolder.time.text = chatsModel.date
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var message = itemView.findViewById<TextView>(R.id.message)
        internal var time = itemView.findViewById<TextView>(R.id.time)



    }
}