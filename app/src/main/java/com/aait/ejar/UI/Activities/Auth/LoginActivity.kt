package com.aait.ejar.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.util.Log
import android.widget.*
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Models.UserResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Main.MainActivity
import com.aait.ejar.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_login

    lateinit var phone: EditText
    lateinit var password: EditText
    lateinit var view: ImageView
    lateinit var forgot_pass: TextView
    lateinit var login: Button
    lateinit var register: LinearLayout
    lateinit var visitor:TextView
    var deviceID = ""

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        forgot_pass = findViewById(R.id.forgot)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        visitor = findViewById(R.id.visitor)
        register.setOnClickListener { startActivity(Intent(this,RegisterActivity::class.java)) }
        forgot_pass.setOnClickListener {startActivity(Intent(this,ForgotPassActivity::class.java))  }
        visitor.setOnClickListener { user.loginStatus= false
        startActivity(Intent(this,MainActivity::class.java))
        finish()}
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }

        login.setOnClickListener {
            if(CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                Login()
            }
        }

    }

    fun Login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(phone.text.toString()
                ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type!!.equals("user")) {
                            user.loginStatus = true
                            user.userData = response.body()?.data!!

                            val intent = Intent(this@LoginActivity, MainActivity::class.java)

                            startActivity(intent)
                            finish()

                        }else{

                        }
//                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                        startActivity(intent)
//                        finish()
                    }else if (response.body()?.value.equals("2")){
                        val intent = Intent(this@LoginActivity,ActivateCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}