package com.aait.ejar.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.ejar.Base.ParentRecyclerAdapter
import com.aait.ejar.Base.ParentRecyclerViewHolder
import com.aait.ejar.Models.OrderModel
import com.aait.ejar.R
import com.bumptech.glide.Glide

class OrderAdapter (context: Context, data: MutableList<OrderModel>, layoutId: Int) :
        ParentRecyclerAdapter<OrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.category.text = mcontext.getString(R.string.category_type)+":"+questionModel.category
        viewHolder.num.text = mcontext.getString(R.string.order_num)+":"+questionModel.id
        viewHolder.time.text = questionModel.created
        Glide.with(mcontext).asBitmap().load(questionModel.avatar).into(viewHolder.image)


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)



        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var category=itemView.findViewById<TextView>(R.id.category)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var num = itemView.findViewById<TextView>(R.id.num)
        internal var time = itemView.findViewById<TextView>(R.id.time)





    }
}