package com.aait.ejar.UI.Activities.AppInfo

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Models.CallUsResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Main.MainActivity
import com.aait.ejar.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallUsActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_call_us
    lateinit var back:ImageView
    lateinit var title:TextView


    lateinit var twitter:ImageView
    lateinit var face:ImageView
    lateinit var insta:ImageView
    lateinit var whatsapp:ImageView
    lateinit var message:EditText
    lateinit var confirm:Button
    lateinit var name:EditText
    lateinit var phone:EditText

    var twit = ""
    var facebook = ""
    var instagram = ""
    var whats = ""

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        message = findViewById(R.id.message)
        whatsapp = findViewById(R.id.whats)
        confirm = findViewById(R.id.confirm)
        twitter = findViewById(R.id.twitter)
        face = findViewById(R.id.face)
        insta = findViewById(R.id.insta)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish() }
        title.text = getString(R.string.contact_us)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.user)
        getData(null)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(message,getString(R.string.write_your_message))){
                return@setOnClickListener
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.CallUs(lang.appLanguage,message.text.toString(),name.text.toString(),phone.text.toString())?.enqueue(object:
                        Callback<CallUsResponse> {
                    override fun onFailure(call: Call<CallUsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<CallUsResponse>, response: Response<CallUsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if(response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext,getString(R.string.sent_done))
                              onBackPressed()
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
        }

        face.setOnClickListener { if (facebook.startsWith("http"))
        {
            Log.e("here", "333")
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(facebook)
            startActivity(i)

        } else {
            Log.e("here", "4444")
            val url = "https://$facebook"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        }

        twitter.setOnClickListener { if (!twit.equals(""))
        {
            if (twit.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(twit)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$twit"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        insta.setOnClickListener { if (!instagram.equals(""))
        {
            if (instagram.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(instagram)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$instagram"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }  }
        whatsapp.setOnClickListener { if (!whats.equals("")){
            openWhatsAppConversationUsingUri(mContext!!,whats!!.replaceFirst("0","+966",false),"")

        } }

    }

    fun getData(mess:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CallUs(lang.appLanguage,mess,null,null)?.enqueue(object:
            Callback<CallUsResponse> {
            override fun onFailure(call: Call<CallUsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CallUsResponse>, response: Response<CallUsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){

                        for(i in 0..response.body()?.socials?.size!!-1){
                            if(response.body()?.socials?.get(i)?.name.equals("snapchat")){
                                facebook = response.body()?.socials?.get(i)?.link!!

                            }else if(response.body()?.socials?.get(i)?.name.equals("twitter")){
                                twit = response.body()?.socials?.get(i)?.link!!
                            }
                            else if(response.body()?.socials?.get(i)?.name.equals("instagram")){
                                instagram = response.body()?.socials?.get(i)?.link!!
                            }else if(response.body()?.socials?.get(i)?.name.equals("whatsapp")){
                                whats = response.body()?.socials?.get(i)?.link!!
                            }
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun openWhatsAppConversationUsingUri(
            context: Context,
            numberWithCountryCode: String,
            message: String
    ) {

        val uri =
                Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }
}