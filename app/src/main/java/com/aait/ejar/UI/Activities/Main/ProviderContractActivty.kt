package com.aait.ejar.UI.Activities.Main

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.widget.*
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.Models.TermsResponse
import com.aait.ejar.Network.Client
import com.aait.ejar.Network.Service
import com.aait.ejar.R
import com.aait.ejar.Utils.CommonUtil
import com.aait.ejar.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProviderContractActivty : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_contract
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var id_num: EditText
    lateinit var date: TextView
    lateinit var send: Button
    lateinit var account_num:EditText
    lateinit var image:TextView
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var d = ""
    var order_id = 0
    override fun initializeComponents() {
        order_id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        id_num = findViewById(R.id.id_num)
        date = findViewById(R.id.date)
        send = findViewById(R.id.send)
        account_num = findViewById(R.id.account_number)
        image = findViewById(R.id.image)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.contract_info)
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        date.setOnClickListener {
            val c = Calendar.getInstance()
            val Year = c.get(Calendar.YEAR)
            val Month = c.get(Calendar.MONTH)
            val Day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                date.text = ("" + dayOfMonth + "/" + m + "/" + year)

            }, Year, Month, Day)
            dpd.show()
        }
        send.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(id_num,getString(R.string.enter_id_number))||
                    CommonUtil.checkLength(id_num,getString(R.string.id_number),10)||
                    CommonUtil.checkTextError(date,getString(R.string.date_of_birth))){
                return@setOnClickListener
            }else{
                Send()
            }
        }
    }

    fun Send(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.ProviderContract("Bearer"+user.userData.token,lang.appLanguage
                ,order_id,phone.text.toString(),name.text.toString(),id_num.text.toString(),date.text.toString())?.enqueue(object : Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@ProviderContractActivty,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
       if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                image.text = getString(R.string.images_attached)

                if (ImageBasePath != null) {
                    image.text = getString(R.string.images_attached)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }
}