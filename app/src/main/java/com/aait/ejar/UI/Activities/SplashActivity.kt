package com.aait.ejar.UI.Activities

import android.content.Intent
import android.os.Handler
import androidx.appcompat.app.AppCompatDelegate
import com.aait.ejar.Base.ParentActivity
import com.aait.ejar.R
import com.aait.ejar.UI.Activities.Main.MainActivity

class SplashActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash

    var isSplashFinishid = false
    override fun initializeComponents() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){

//                    val intent = Intent(this, MainActivity::class.java)
//
//                    startActivity(intent)
                 //   this@SplashActivity.finish()
//                        var intent = Intent(this@SplashActivity, PreActivity::class.java)
//                        startActivity(intent)
//                        finish()
                    var intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()

                }else {
                    var intent = Intent(this@SplashActivity, LangActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, 2100)
        }, 1800)
    }


}
